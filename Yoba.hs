module Yoba where

import Control.Lens
import Data.Proxy
import Data.Vinyl

data Tag1

data Tag2

data Tag3

tag1 :: Proxy Tag1
tag1 = Proxy

tag2 :: Proxy Tag2
tag2 = Proxy

tag3 :: Proxy Tag3
tag3 = Proxy

type family RecTf1 tag :: * where
  RecTf1 Tag1 = Int
  RecTf1 Tag2 = String

newtype Rec1 tag = Rec1
  { unRec1 :: RecTf1 tag
  }
deriving instance Show (RecTf1 tag) => Show (Rec1 tag)

type family RecTf2 tag :: * where
  RecTf2 Tag1 = Double
  RecTf2 Tag2 = Integer
  RecTf2 Tag3 = Bool

newtype Rec2 tag = Rec2
  { unRec2 :: RecTf2 tag
  }
deriving instance Show (RecTf2 tag) => Show (Rec2 tag)

x :: Rec Rec1 '[Tag1, Tag2]
x = ((Rec1 10) :& RNil) <+> ((Rec1 "10") :& RNil)

y :: Rec Rec2 '[Tag2, Tag3]
y = (Rec2 10) :& (Rec2 True) :& RNil


class Wrap recw where
  type WType recw :: *
  wrap :: WType recw -> recw
  unwrap :: recw -> WType recw

instance Wrap (Rec1 tag) where
  type WType (Rec1 tag) = RecTf1 tag
  wrap = Rec1
  unwrap = unRec1

instance Wrap (Rec2 tag) where
  type WType (Rec2 tag) = RecTf2 tag
  wrap = Rec2
  unwrap = unRec2

field
  :: (Wrap (r tag))
  => proxy tag
  -> (WType (r tag))
  -> Rec r '[tag]
field _ repr = (wrap repr) :& RNil

-- Yoba.hs:70:16: Warning:
--     Found hole `_' with type: '[Tag1, Tag2]
--     In the type signature for `xx': Rec Rec1 _
xx :: Rec Rec1 _
xx = field tag1 10 <+> field tag2 "10"

-- Yoba.hs:77:16: Warning:
--     Found hole `_' with type: '[Tag1, Tag2, Tag3]
--     In the type signature for `yy': Rec Rec2 _
yy :: Rec Rec2 _
yy  = field tag1 10
  <+> field tag2 20
  <+> field tag3 True
